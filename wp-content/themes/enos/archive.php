<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();


$bg = get_field("news_bg_image", "option");
$icon = get_field("news_icon", "option");

?>

<div class="wrapper" id="archive-wrapper">

	<!-- subpages hero -->
	<section  style="<?php if($bg != false && $bg != null): ?>background: url('<?php echo $bg['url'] ?>');<?php endif; ?>"   class="section-hero-subpages">
			<div class="hero-center-subpages">

						<?php if($icon != null && $icon != false): ?>
							<img src="<?php echo $icon['url']; ?>" />
						<?php endif; ?>
						<h1><?php echo get_field("news_title","option"); ?></h1>
						<p>
							<?php echo get_field("news_subtitle","option"); ?>
						</p>

			</div>
	</section>
	<!-- /subpages hero -->

	<div class="container" id="content" tabindex="-1">

		<div class="row">


			<div class="col-md-4">
					<?php get_template_part( '/sidebar-templates/sidebar-storitve' ); ?>
			</div>


						      <div class="col-md-8">



			<!-- Do the left sidebar check -->
			<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

			<main class="site-main" id="main">

				<?php if ( have_posts() ) : ?>


					<?php /* Start the Loop */ ?>
					<?php while ( have_posts() ) : the_post(); ?>


						<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

							<header class="entry-header">

								<?php
								the_title(
									sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ),
									'</a></h2>'
								);
								?>
								<div class="datum"><?php echo get_the_date(); ?></div>
								<?php if ( 'post' == get_post_type() ) : ?>

									<div class="entry-meta">
										<?php understrap_posted_on(); ?>
									</div><!-- .entry-meta -->

								<?php endif; ?>

							</header><!-- .entry-header -->

							<?php echo get_the_post_thumbnail( $post->ID, 'large' ); ?>

							<div class="entry-content">

								<?php the_excerpt(); ?>

								<?php
								wp_link_pages(
									array(
										'before' => '<div class="page-links">' . __( 'Pages:', 'understrap' ),
										'after'  => '</div>',
									)
								);
								?>

							</div><!-- .entry-content -->

							<footer class="entry-footer">

								<?php understrap_entry_footer(); ?>

							</footer><!-- .entry-footer -->

						</article><!-- #post-## -->


					<?php endwhile; ?>

				<?php else : ?>

				<?php endif; ?>

			</main><!-- #main -->

			<!-- The pagination component -->
			<?php understrap_pagination(); ?>



					 </div>
		</div> <!-- .row -->

	</div><!-- #content -->

	</div><!-- #archive-wrapper -->

<?php get_footer(); ?>
